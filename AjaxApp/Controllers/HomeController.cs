﻿using AjaxApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AjaxApp.Controllers
{
    public class HomeController : Controller
    {
        Db_CustomerEntities db = new Db_CustomerEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About(Tbl_User user)
        {
            user = db.Tbl_User.Where(x => x.Id_User == 2).SingleOrDefault();
            //return Json(user, JsonRequestBehavior.AllowGet);

            //var chk = new check
            //{
            //    subject = "hello!" + param1,
            //    description = param2 + "Years Old"
            //};

            //ViewBag.Message = "Your application description page.";

            //return JsonConvert.SerializeObject(chk);
            return View(Json(user, JsonRequestBehavior.AllowGet));
        }

        public class check
        {
            public string subject { get; set; }
            public string description { get; set; }
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}